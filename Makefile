CC = g++
CFLAGS = -c -Wall
LDFLAGS =
INCLUDE = project/include

C_SRC = client_main client_chat client 
C_SOURCES = $(addsuffix .cpp, $(addprefix project/src/, $(C_SRC)))
C_OBJECTS = $(C_SOURCES:.cpp=.o)
C_EXECUTABLE = client.out

S_SRC = player scene polygon chat server session server_main 
S_SOURCES = $(addsuffix .cpp, $(addprefix project/src/, $(S_SRC)))
S_OBJECTS = $(S_SOURCES:.cpp=.o)
S_EXECUTABLE = server.out

client: $(C_SOURCES) $(C_EXECUTABLE)
server: $(S_SOURCES) $(S_EXECUTABLE)

$(C_EXECUTABLE): $(C_OBJECTS)
	$(CC) $(C_OBJECTS) -o $@ -DSFML_STATIC \
	-Iinclude -Llib -pthread -lsfml-graphics -lsfml-system -lsfml-window -lsfml-network -lfreetype -lstdc++

$(S_EXECUTABLE): $(S_OBJECTS)
	$(CC) $(S_OBJECTS) -o $@ -DSFML_STATIC \
	-Iinclude -Llib -pthread -lsfml-graphics -lsfml-system -lsfml-window -lsfml-network -lfreetype -lstdc++

%.o: %.cpp
	g++ -c -o $@ $< -I$(INCLUDE) 

clear:
	rm -f project/src/*.o $(C_EXECUTABLE) $(S_EXECUTABLE)
