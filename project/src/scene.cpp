#include "scene.h"
#include <iostream>

Scene::Scene(){}

Scene::~Scene(){}

void Scene::addPlayer(Player *pl)
{
    _players.push_back(*pl);
}

void Scene::addPolygon(sf::Vector2f first, sf::Vector2f second, sf:: Vector2f third)
{
    Polygon pg(first, second, third);
    _polygons.push_back(pg);
}

static bool isCollision(Player pl, sf::Vector2f move, Polygon pg)
{
    sf::Vector2f v = {pl.getPosition().x + move.x, pl.getPosition().y + move.y};
    return pg.isDotInPolugon(v) || 
           pg.isDotInPolugon(sf::Vector2f(v.x, v.y + pl.getSize().y)) ||
           pg.isDotInPolugon(sf::Vector2f(v.x + pl.getSize().x, v.y)) || 
           pg.isDotInPolugon(sf::Vector2f(v.x + pl.getSize().x, v.y + pl.getSize().y));
}

void Scene::movePlayerTo(float x, float y, Player *pl)
{
    float lenghtOffset = sqrt(x*x + y*y); 
    float speed = 1.0;
    bool out = true;
    //collision with walls
    for (Polygon pg : _polygons)
    {
        if (isCollision(*pl, sf::Vector2f(speed * x / lenghtOffset, speed * y / lenghtOffset), pg))
        {
            out = false;
            break;
        }
    }

    if (lenghtOffset > 10e-4 && out == true)
    {
        pl->move(sf::Vector2f(speed * x / lenghtOffset, speed * y / lenghtOffset));
    }

}

std::list<Player>* Scene::getListPlayers()
{
    return &_players;
}

Chat& Scene::getChat()
{
    return _chat;
}

bool Scene::isFull()
{
    int full = 2;
    return full == _players.size();
}
