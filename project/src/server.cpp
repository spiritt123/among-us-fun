
#include "server.h"

Server::Server(unsigned short port)
{
    _listner.listen(port);
    _listner.setBlocking(false);
}

Server::~Server() {}

void Server::run()
{
    sf::TcpSocket *nextClient = nullptr;

    int elapsed = 0;
    while (1)
    {
        if (nextClient == nullptr)
        {
            nextClient = new sf::TcpSocket;
            nextClient->setBlocking(false);
        }
 
        if (_listner.accept(*nextClient) == sf::Socket::Done)
        {
            pushPlayerInFreeSession(nextClient);
            nextClient = nullptr;
        }
 
        for (Session &session : _sessions)
        {
            session.update();
        }
    
        deleteDeadSession();

        if (elapsed > 50000)
            return;
        ++elapsed;
        usleep(500);
    }
}

void Server::pushPlayerInFreeSession(sf::TcpSocket *nextClient)
{
    for (Session &session : _sessions)
    {
        if (session.isNotFull() && !session.isDead())
        {
            session.addPlayer(nextClient);
            return;
        }
    }
    Session session;
    session.run();
    _sessions.push_back(session);
    _sessions.back().addPlayer(nextClient);
}

void Server::deleteDeadSession()
{
    for (std::list<Session>::iterator it = _sessions.begin(); it != _sessions.end(); )
    {
        if (it->isDead())
        {
            it = _sessions.erase(it);
            if (_sessions.size() == 0)
            {
                std::cout << "0 session";
            }
        }
        else
        {
            ++it;
        }
    }
}
