
#include "server.h"
#include <iostream>

Session::Session()
{
    _freeId = 1;
    _isDead = false;
}


Session::~Session() {}

void Session::addPlayer(sf::TcpSocket *nextClient)
{ 
    Player *pl = new Player;
    pl->connect(nextClient);
    pl->setId(_freeId);
    _scene.addPlayer(pl);
    ++_freeId;
}


void Session::update()
{
    //handle incoming data
    std::list<Player> *clients = _scene.getListPlayers();
    for (std::list<Player>::iterator it = clients->begin(); it != clients->end(); )
    {
        sf::Packet packet;
        sf::Socket::Status status = it->getSocket()->receive(packet);

        std::string str;
        switch (status)
        {
        case sf::Socket::Done:
            packet >> str;
            if (str == "players")
            {
                float x, y;
                packet >> x >> y;
                _scene.movePlayerTo(x, y, &*it);
                broadCast(&*it);
            }
            else if (str == "chat")
            {
                packet >> str;
                _scene.getChat().addLine(std::to_string(it->getId()) + " : " + str);
                broadCastChat();
            }
            ++it;
            break;

        case sf::Socket::Disconnected:
            it = _scene.getListPlayers()->erase(it);
            if (_scene.getListPlayers()->size() == 0)
            {
                _isDead = true;
            }
            break;
        default:
            ++it;
        }
    }
}

void Session::broadCast(Player *pl)
{
    sf::Packet packetOfData;
    packetOfData << "players";
    packetOfData << (int)(_scene.getListPlayers()->size());

    for (std::list<Player>::iterator it = _scene.getListPlayers()->begin(); 
        it!=_scene.getListPlayers()->end(); ++it)
    {
        packetOfData << it->getPosition().x << it->getPosition().y;
    }

    pl->getSocket()->send(packetOfData);
}

void Session::broadCastChat()
{
    std::list<Player> *clients = _scene.getListPlayers();
    for (std::list<Player>::iterator it = clients->begin(); it != clients->end(); ++it)
    {
        sf::Packet packetOfData;
        packetOfData << "chat";

        int size = _scene.getChat().getHistory().size();
        packetOfData << size;
        for (int i = 0; i < size; ++i)
        {
            packetOfData << _scene.getChat().getHistory()[i];
        }
        it->getSocket()->send(packetOfData);    
    }
}

void Session::run()
{
    srand(time(NULL));

    _scene.addPolygon(sf::Vector2f(50, 10), sf::Vector2f(50, 180), sf::Vector2f(5, 180));
    _scene.addPolygon(sf::Vector2f(200, 100), sf::Vector2f(500, 180), sf::Vector2f(450, 180));
}

bool Session::isNotFull()
{
    return !_scene.isFull();
}

bool Session::isDead()
{
    return _isDead;
}
