
#include "player.h"
#include <iostream>

Player::Player()
{
    _position = {180, 130};
    _h = 100;
    _w = 30;
}

Player::~Player()
{

}

sf::Vector2f Player::getPosition()
{
    return _position;
}

void Player::setPosition(sf::Vector2f position)
{
    _position = position;
}

sf::Vector2f Player::getSize()
{
    return sf::Vector2f(_w, _h);
}

void Player::setSize(sf::Vector2f v)
{
    _w = v.x;
    _h = v.y;
}

void Player::move(sf::Vector2f move)
{
    _position.x += move.x;
    _position.y += move.y;
}

void Player::connect(sf::TcpSocket *socket)
{
    _socket = socket;
}

sf::TcpSocket* Player::getSocket()
{
    return _socket;
}

void Player::setId(int id)
{
    _id = id;
}

int  Player::getId()
{
    return _id;
}

