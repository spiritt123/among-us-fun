
#include "polygon.h"

Polygon::Polygon(sf::Vector2f first, sf::Vector2f second, sf:: Vector2f third)
{
    this->first = first;
    this->second = second;
    this->third = third;
}

Polygon::~Polygon()
{

}

static double g(sf::Vector2f a, sf::Vector2f b, sf::Vector2f d) {
    return (d.x - a.x) * (b.y - a.y) - (d.y - a.y) * (b.x - a.x);
}

static bool f(sf::Vector2f a, sf::Vector2f b, sf::Vector2f c, sf::Vector2f d) {
    return g(a, b, c) * g(a, b, d) >= 0;
}

bool Polygon::isDotInPolugon(sf::Vector2f dot)
{
    return f(first, second, third, dot) && f(second, third, first, dot) && f(third, first, second, dot);
}
