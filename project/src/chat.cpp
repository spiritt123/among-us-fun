#include "chat.h"

Chat::Chat()
{
    _position = sf::Vector2f(0, 0);
    _size = sf::Vector2f(800, 600);
    _history.clear();
}


Chat::~Chat(){}


void Chat::addLine(std::string str)
{
    _history.push_back(str);
}

std::string Chat::getMessage()
{
    return _message;
}

std::vector<std::string> Chat::getHistory()
{
    return _history;
}

