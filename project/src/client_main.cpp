
#include <SFML/Graphics.hpp>
#include <SFML/Network.hpp>
#include <SFML/Window.hpp>
#include <iostream>
#include "unistd.h"

#include "client.h"
#include "client_chat.h"

#define WIDTH 800
#define HEIGHT 600

void renderPlayers(sf::RenderWindow &window, sf::Packet *packetOfData);

int main() 
{
    Client client;

    sf::Socket::Status status = client.connect(sf::IpAddress::getLocalAddress(), 8080);

    if (status != sf::Socket::Done)
    {
        std::cout<<"Sorry we couldn't !!! connect\n";
        return -1;
    }


    sf::RenderWindow window(sf::VideoMode(WIDTH, HEIGHT), "Among Ass");
    window.setFramerateLimit(30);
    
    ClientChat chat(&window);
    float x, y;

    while (window.isOpen())
    {
        sf::Event event;
        while (window.pollEvent(event))
        {
            if (event.type == sf::Event::Closed)
            {
                window.close();
            }
            if (event.type == sf::Event::MouseMoved)
            {
                x = event.mouseMove.x - (float)WIDTH/2;
                y = event.mouseMove.y - (float)HEIGHT/2;
            } 
            if (event.type == sf::Event::KeyPressed)
            {
                if (event.key.code == sf::Keyboard::Escape) window.close();
            }

            //chat.updateMessage(event, client);
        }
        
        status = client.send(x, y);
        if (status == sf::Socket::Disconnected)
        {
            return 0;
        }
       
        
        sf::Packet *packetOfData = new sf::Packet;
        status = client.receive(packetOfData);

        if (status == sf::Socket::Done)
        {
            window.clear();
            
            std::string datatype;

            *packetOfData >> datatype;

            if (datatype == "players")
            {
                renderPlayers(window, packetOfData);
            }
            else if (datatype == "chat")
            {
                chat.correctHistory(packetOfData);
            }
            
        }
        else if (status == sf::Socket::Disconnected)
        {
            return 0;
        }
        
        usleep(1000);
        window.display();
    }
 
    return 0;
}

void renderPlayers(sf::RenderWindow &window, sf::Packet *packetOfData)
{
    int k;
    *packetOfData >> k;
    
    float px, py;
    for (int i = 0; i < k; ++i)
    {
        *packetOfData >> px >> py;

        sf::RectangleShape rectangle(sf::Vector2f(100, 50));
        rectangle.setPosition(sf::Vector2f(px, py));
        std::cout << ">" << px << ":" << py << "\n";

        window.draw(rectangle);
        usleep(10);
    }
}

