
#include "client_chat.h"

ClientChat::ClientChat(sf::RenderWindow *window)
{
    _wnd = window;
    _size = sf::Vector2f(800, 600);
}

ClientChat::~ClientChat()
{
    //delete _wnd;
}

void ClientChat::updateMessage(sf::Event &e, Client &client)
{
    if(e.type == sf::Event::TextEntered)
	{
		char code = static_cast<char>(e.text.unicode);

		if(e.text.unicode == 13)//enter
		{
            client.send(_message);
			_message.clear();
		}
		else if(code != '\b')
        {
			_message.push_back(code);
        }
        else if(code == '\b')
		{
			if(_message.size() > 0)
            {
				_message.pop_back();
            }
		}
	}
    draw();
}

void ClientChat::correctHistory(sf::Packet *packetOfData)
{
    int size;

    *packetOfData >> size;

    std::string line;
    
    if (size != _history.size())
    {
        _history.clear();
        for (int i = 0; i < size; ++i)
        {
            *packetOfData >> line;
            _history.push_back(line);
        }
    }
    draw();
}

void ClientChat::draw()
{
    sf::RectangleShape background;
    background.setSize(_size);
    background.setFillColor(sf::Color::Blue);
    background.setPosition(0.f, 0.f);
    _wnd->draw(background);

    sf::RectangleShape cell;
    cell.setSize(sf::Vector2f(_size.x, _h_text));
    cell.setFillColor(sf::Color::Red);
    cell.setPosition(0.f, _size.y - _h_text);
    _wnd->draw(cell);
    
    sf::Font font;
    font.loadFromFile("Abbieshire.ttf");
    
    sf::Text text;
    text.setString(_message);
    text.setFont(font);
    text.setFillColor(sf::Color::White);
    text.setCharacterSize(16);
    text.setPosition(0.f, _size.y - _h_text);
    _wnd->draw(text);

    for (int i = 0; i < _history.size(); ++i)
    {
        cell.setSize(sf::Vector2f(_size.x, _h_text));
        cell.setFillColor(sf::Color::Red);
        cell.setPosition(0.f, _size.y - _h_text * i - 2 * _h_text);
        _wnd->draw(cell);

        text.setString(_history[_history.size() - 1 - i]);
        text.setFont(font);
        text.setFillColor(sf::Color::White);
        text.setCharacterSize(16);
        text.setPosition(0.f, _size.y - _h_text * i - 2 * _h_text);
        _wnd->draw(text);
    }
}
