#pragma once
#include <SFML/Graphics.hpp>

class Session
{
public:
    Session();
    ~Session();
    
    void run();

    void addPlayer(sf::TcpSocket *nextClient);
    void update();

    bool isNotFull();

    bool isDead();

private:
    void broadCast(Player *pl);
    void broadCastChat();

private:
    Scene _scene;

    int  _freeId;
    bool _isDead;
};
