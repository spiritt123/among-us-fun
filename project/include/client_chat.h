#pragma once

#include <SFML/Graphics.hpp>
#include <iostream>

#include "client.h"

class ClientChat
{
public:
    ClientChat(sf::RenderWindow *window);
    ~ClientChat();

    void updateMessage(sf::Event &e, Client &client);

    void correctHistory(sf::Packet *packetOfData);

    void draw();

private:
    sf::RenderWindow *_wnd;
    sf::Vector2f _size;
    float _h_text = 20.f;
    std::vector<std::string> _history;
    std::string _message;


};
