#pragma once

#include <SFML/Graphics.hpp>
#include <SFML/Network.hpp>

class Player
{
public:
    Player();
    ~Player();
    
    sf::Vector2f getPosition();
    void setPosition(sf::Vector2f position);
    
    sf::Vector2f getSize();
    void setSize(sf::Vector2f v);
    void move(sf::Vector2f move);

    void connect(sf::TcpSocket *socket);
    sf::TcpSocket* getSocket();

    void setId(int id);
    int  getId();

private:
    sf::Vector2f _position;
    sf::TcpSocket *_socket;
    float _h, _w;
    int _id;
};

