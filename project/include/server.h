#pragma once

#include <iostream>

#include "SFML/Network.hpp"

#include "scene.h"
#include "chat.h"
#include "session.h"
#include "unistd.h"

class Server
{
public:
    Server(unsigned short port);
    ~Server();

    void run();
    void pushPlayerInFreeSession(sf::TcpSocket *nextClient);

    void deleteDeadSession();

private:
    sf::TcpListener _listner;
    
    std::list<Session> _sessions;
};
