#pragma once

#include <SFML/Graphics.hpp>

class Polygon
{
public:
    Polygon(sf::Vector2f first, sf::Vector2f second, sf:: Vector2f third);
    ~Polygon();

    bool isDotInPolugon(sf::Vector2f dot);

public:
    sf::Vector2f first, second, third;

private:
};

