#pragma once

#include <SFML/Graphics.hpp>
#include "player.h"
#include "polygon.h"
#include "chat.h"
#include <cmath>
#include <list>

class Scene
{
public:
    Scene();
    ~Scene();

    void addPlayer(Player *pl);
    void addPolygon(sf::Vector2f first, sf::Vector2f second, sf::Vector2f third);
    void draw(float x, float y);
    void movePlayerTo(float x, float y, Player *pl);

    std::list<Player>* getListPlayers();

    Chat& getChat();
    
    bool isFull();

private:
    void drawPlayers(float x, float y);
    void drawChat();

private:
    std::list<Player> _players;
    std::list<Polygon> _polygons; 
    Chat _chat;
};


