#pragma once

#include <SFML/Graphics.hpp>
#include <iostream>

class Chat
{
public:
    Chat();
    ~Chat();
    
    void addLine(std::string str);

    std::string getMessage();

    std::vector<std::string> getHistory();

private:
    sf::Vector2f _position;
    sf::Vector2f _size;
    std::vector<std::string> _history;
    std::string _message;
};

